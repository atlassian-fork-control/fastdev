#set($triggerInstructionsWithHtml = $i18n.getText("fastdev.trigger.instructions", $i18n.getText("fastdev.scan.and.reload")))

<!DOCTYPE HTML>
<html>
<head>
    <title>$i18n.getText("fastdev.title")</title>

    <link type="text/css" href="$baseurl/download/resources/com.atlassian.labs.fastdev-plugin/css/fastdev.css" rel="stylesheet" />
    
    <script src="$baseurl/download/resources/com.atlassian.labs.fastdev-plugin/js/jquery-1.4.4.js" type="text/javascript"></script>
    <script src="$baseurl/download/resources/com.atlassian.labs.fastdev-plugin/js/util.js" type="text/javascript"></script>
    <script src="$baseurl/download/resources/com.atlassian.labs.fastdev-plugin/js/template.js" type="text/javascript"></script>
    <script src="$baseurl/download/resources/com.atlassian.labs.fastdev-plugin/js/fastdev.js" type="text/javascript"></script>

    <meta name="fastdev.version" content="$version">
    <meta name="application-base-url" content="$baseurl">
    <meta name="fastdev.expand" content="$i18n.getText("fastdev.expand")">
    <meta name="fastdev.collapse" content="$i18n.getText("fastdev.collapse")">
    <meta name="fastdev.installing" content="$i18n.getText("fastdev.installing")">
    <meta name="fastdev.install.success" content="$i18n.getText("fastdev.install.success")">
    <meta name="fastdev.install.failure" content="$i18n.getText("fastdev.install.failure")">
    <meta name="fastdev.error.code" content="$i18n.getText("fastdev.error.code")">
    <meta name="fastdev.time.remaining" content="$i18n.getText("fastdev.time.remaining")">
    <meta name="fastdev.time.remaining.singular" content="$i18n.getText("fastdev.time.remaining.singular")">
    <meta name="fastdev.time.slower" content="$i18n.getText("fastdev.time.slower")">
    <meta name="fastdev.time.slower.singular" content="$i18n.getText("fastdev.time.slower.singular")">
    <meta name="fastdev.reloading.plugins" content="$i18n.getText("fastdev.reloading.plugins")">
    <meta name="fastdev.reloading.plugins.singular" content="$i18n.getText("fastdev.reloading.plugins.singular")">
    <meta name="fastdev.reloading.plugins.none" content="$i18n.getText("fastdev.reloading.plugins.none")">
    <meta name="fastdev.building.from" content="$i18n.getText("fastdev.building.from")">
    <meta name="fastdev.error.server" content="$i18n.getText("fastdev.error.server")">
    <meta name="fastdev.trigger.control.reload" content="$i18n.getText("fastdev.trigger.control.reload")">

</head>
<body>
    <div id="header">
        <h1 id="fastdev-header" title="$i18n.getText("fastdev.title")">$i18n.getText("fastdev.title")</h1>
    </div>
    <div id="content" class="loading">

        <div id="fastdev-loading">
            <h2><span class="loading-text">$i18n.getText("fastdev.scanning")</span></h2>
        </div>

        <div id="fastdev-no-changes">
            <h2>$i18n.getText("fastdev.no.changes.title")</h2>
            <button class="trigger-reload toolbar-button">$i18n.getText("fastdev.scan.and.reload")</button>
            <div class="section">
                <p>
                    $i18n.getText("fastdev.no.changes.description")
                </p>
            </div>
            <h2>$i18n.getText("fastdev.about.title")</h2>
            <div class="section">
                <p>
                    $i18n.getText("fastdev.about.description")
                </p>
                <p>
                    $triggerInstructionsWithHtml
                </p>
            </div>
        </div>

        <div id="fastdev-tasks">
            <h2 id="fastdev-tasks-header"></h2>
            <button class="trigger-reload toolbar-button disabled" disabled="disabled">$i18n.getText("fastdev.scan.and.reload")</button>
        </div>
        
    </div>
    <div id="footer">
        $i18n.getText("fastdev.footer", "$version") |
        <a href="https://studio.atlassian.com/browse/FASTDEV">$i18n.getText("fastdev.footer.bugs")</a> |
        <a href="https://studio.atlassian.com/wiki/display/FASTDEV">$i18n.getText("fastdev.footer.docs")</a>
    </div>

    <script type="text/x-template" id="task-template">
        <div class="task section">
            <div class="progress-header">
                <span class="progress-icon"></span>
                <h3 class="plugin-name"></h3>
                <div class="task-status"></div>
                <div class="task-progress">
                    <div class="progress-bar"></div>
                    <div class="progress-text"></div>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="maven-output-container">
                <div class="maven-output"></div>
                <div class="handle"></div>
            </div>
        </div>
    </script>
</body>
</html>