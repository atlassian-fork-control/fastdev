var Fastdev = Fastdev || {};

Fastdev.util = Fastdev.util || (function() {

    var params = {};

    /**
     * Find parameters in the DOM and store them in the ajs.params object.
     */
    function populateParameters() {
        var ajs = this;
        $('meta').each(function () {
            var name = this.name,
                value = this.content;
            ajs.params[name] = (value.match(/^(tru|fals)e$/i) ? value.toLowerCase() == "true" : value);
        });
    }

    /**
     * Replaces tokens in a string with arguments, similar to Java's MessageFormat.
     * Tokens are in the form {0}, {1}, {2}, etc.
     * @method format
     * @param message the message to replace tokens in
     * @param arg (optional) replacement value for token {0}, with subsequent arguments being {1}, etc.
     * @return {String} the message with the tokens replaced
     * @usage AJS.format("This is a {0} test", "simple");
     */
    function format(message) {
        var token = /^((?:(?:[^']*'){2})*?[^']*?)\{(\d+)\}/, // founds numbers in curly braces that are not surrounded by apostrophes
            apos = /'(?!')/g; // founds "'", bot not "''"
        // we are caching RegExps, so will not spend time on recreating them on each call
        Fastdev.util.format = function (message) {
            var args = arguments,
                res = "",
                match = message.match(token);
            while (match) {
                message = message.substring(match[0].length);
                res += match[1].replace(apos, "") + (args.length > ++match[2] ? args[match[2]] : "");
                match = message.match(token);
            }
            return res += message.replace(apos, "");
        };
        return Fastdev.util.format.apply(Fastdev.util, arguments);
    }

    $(function () {Fastdev.util.populateParameters();});

    return {
        params: params,
        format: format,
        populateParameters: populateParameters
    }
})();