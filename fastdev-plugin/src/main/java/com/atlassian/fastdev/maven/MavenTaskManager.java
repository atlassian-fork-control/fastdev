package com.atlassian.fastdev.maven;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import com.atlassian.fastdev.ScanResult;
import com.atlassian.fastdev.util.Either;
import com.atlassian.fastdev.util.Pair;

import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapMaker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import static com.atlassian.fastdev.FastdevProperties.FASTDEV_MVN_COMMAND;
import static com.atlassian.fastdev.maven.MavenTaskError.INVALID_BUILD_ROOT;
import static com.atlassian.fastdev.maven.MavenTaskError.PLUGIN_INSTALL_IN_PROGRESS;
import static com.atlassian.fastdev.util.Either.left;
import static com.atlassian.fastdev.util.Either.right;
import static com.atlassian.fastdev.util.Option.option;
import static com.atlassian.fastdev.util.Pair.pair;

public class MavenTaskManager implements DisposableBean
{
    private static final long DEFAULT_AVERAGE_BUILD_TIME = 0;
    public static final String ANY_PLUGIN_KEY = "any-plugin-key";
    
    public final Logger LOG = LoggerFactory.getLogger(MavenTaskManager.class);

    private final ExecutorService executorService;

    private final ConcurrentMap<File, MavenTask> inFlightTasks;
    private final ConcurrentMap<File, CLIProcess> cliProcesses;
    private final ConcurrentMap<UUID, MavenTask> tasks;
    private final ConcurrentMap<UUID, MavenTask> completedTasks;
    private final ConcurrentMap<File, RunningAverage> averageBuildTimes;
    private final Iterable<Pair<String, String>> mavenCommands =
        ImmutableList.of(pair("ATLAS_HOME", "/apache-maven/bin/mvn"),
                         pair("M2_HOME", "/bin/mvn"));
    

    public MavenTaskManager()
    {
        this(Executors.newFixedThreadPool(8, new MavenThreadFactory()));
    }

    public MavenTaskManager(ExecutorService executorService)
    {
        this.executorService = executorService;
        this.tasks = new MapMaker().makeMap();
        this.inFlightTasks = new MapMaker().makeMap();
        this.averageBuildTimes = new MapMaker().makeMap();
        this.completedTasks = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).<UUID, MavenTask>build().asMap();
        this.cliProcesses = new MapMaker().makeMap();
    }

    String getMavenCommand()
    {
        for (String command : option(System.getProperty(FASTDEV_MVN_COMMAND)))
        {
            if (new File(command).exists())
            {
                return command;
            }
            else
            {
                LOG.warn("Cannot use maven command " + command + " from property " + FASTDEV_MVN_COMMAND + " because it does not exist");
            }
        }
        for (Pair<String, String> command : mavenCommands)
        {
            for (String path : option(System.getenv(command.first())))
            {
                File cmd = new File(path, command.second());
                if (cmd.exists())
                {
                    return cmd.getAbsolutePath();
                }
                else
                {
                    LOG.warn("Cannot use maven command " + cmd.getAbsolutePath() + " from property " + command.first() + " because it does not exist");
                }
            }
        }
        return "mvn";
    }

    public Either<MavenTaskError, MavenTask> createTask(File buildRoot, List<String> commands)
    {
        if (!buildRoot.isDirectory() || !new File(buildRoot, "pom.xml").exists())
        {
            LOG.error("Failed to execute build: " + buildRoot.getPath() + " is not a maven root");
            return left(INVALID_BUILD_ROOT);
        }

        else
        {
            UUID uuid = UUID.randomUUID();
            MavenTask task = new DefaultMavenTask(uuid, buildRoot, commands,this,executorService);
            return add(task);
       }
    }

    public Either<MavenTaskError, MavenTask> createCliTask(ScanResult result, String command, List<String> cliArgs)
    {
        File buildRoot = result.getBuildRoot();
        if (!buildRoot.isDirectory() || !result.getRootPom().exists())
        {
            LOG.error("Failed to execute build: " + buildRoot.getPath() + " is not a maven root");
            return left(INVALID_BUILD_ROOT);
        }

        else
        {
            UUID uuid = UUID.randomUUID();
            MavenTask task = new CLIMavenTask(uuid, result, cliArgs, command,this,executorService);
            return add(task);
        }
    }

    
    boolean hasCliProcess(File buildRoot)
    {
        return cliProcesses.containsKey(buildRoot);    
    }
    
    CLIProcess getCliProcess(File buildRoot)
    {
        return cliProcesses.get(buildRoot);
    }
    
    void removeCliProcess(File buildRoot, CLIProcess process)
    {
        cliProcesses.remove(buildRoot, process);
    }

    void addCliProcess(File buildRoot, CLIProcess process)
    {
        cliProcesses.put(buildRoot, process);
    }
    
    boolean cliPortTaken(Integer port)
    {
        boolean taken = false;
        for(CLIProcess cli : cliProcesses.values())
        {
            if(cli.getPort().equals(port))
            {
                taken = true;
                break;
            }
        }
        
        return taken;
    }

    public MavenTask getTask(UUID uuid)
    {
        return tasks.get(uuid);
    }

    public MavenTask getTaskForRoot(File file)
    {
        return inFlightTasks.get(file);
    }

    public MavenTask getCompletedTask(UUID uuid)
    {
        return completedTasks.get(uuid);
    }

    public Iterable<MavenTask> getAllTasks()
    {
        return ImmutableList.copyOf(tasks.values());
    }

    private Either<MavenTaskError, MavenTask> add(MavenTask task)
    {

        MavenTask existing = inFlightTasks.putIfAbsent(task.getBuildRoot(), task);

        if (existing != null)
        {
            LOG.warn("Ignoring build: " + task.getBuildRoot().getAbsolutePath() + " as a plugin install is currently in progress");
            return left(PLUGIN_INSTALL_IN_PROGRESS);
        }
        tasks.put(task.getUuid(), task);
        return right(task);
    }

    void remove(MavenTask task)
    {
        inFlightTasks.remove(task.getBuildRoot(),task);
        completedTasks.putIfAbsent(task.getUuid(), task);
        tasks.remove(task.getUuid(),task);
    }

    long averageBuildTime(File buildRoot)
    {
        RunningAverage ave = averageBuildTimes.putIfAbsent(buildRoot, new RunningAverage(0, DEFAULT_AVERAGE_BUILD_TIME));

        if (ave == null)
        {
            return 0;
        }
        return ave.average();
    }

    void recordBuildTime(File buildRoot, long latest)
    {
        boolean replaced = false;
        while (!replaced)
        {
            RunningAverage oldAverage = averageBuildTimes.get(buildRoot);
            replaced = averageBuildTimes.replace(buildRoot, oldAverage, oldAverage.adjust(latest));
        }
    }

    public void destroy()
    {
        executorService.shutdown();
        try
        {
            if (!executorService.awaitTermination(10, TimeUnit.SECONDS))
            {
                LOG.warn("Timed out while waiting for Maven executor to shutdown.");
            }
        }
        catch (InterruptedException e)
        {
            LOG.warn("Interrupted while waiting for Maven executor to shutdown.");
        }
        
        for(CLIProcess process : cliProcesses.values())
        {
            process.getProcess().destroy();
        }
    }

    private static class RunningAverage
    {
        private final int n;
        private final long total;

        public RunningAverage(int n, long total)
        {
            this.n = n;
            this.total = total;
        }

        public long average()
        {
            if (n == 0) return 0;
            return total / n;
        }

        public RunningAverage adjust(long latest)
        {
            return new RunningAverage(n + 1, total + latest);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }

            RunningAverage that = (RunningAverage) o;

            if (n != that.n)
            {
                return false;
            }
            if (total != that.total)
            {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = n;
            result = 31 * result + (int) (total ^ (total >>> 32));
            return result;
        }
    }

    private static class MavenThreadFactory implements ThreadFactory
    {
        private final AtomicLong id = new AtomicLong();

        public Thread newThread(Runnable r)
        {
            Thread thread = new Thread(r);
            thread.setName(String.format("FastDev-MavenTask-%d", id.getAndIncrement()));
            thread.setDaemon(true);
            return thread;
        }
    }
}
