package com.atlassian.fastdev.maven;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.Future;

/**
 * @since version
 */
public interface MavenTask
{

    File getBuildRoot();

    long getAverageTaskTime();

    UUID getUuid();

    Iterable<String> getOutput();

    long getElapsedTime();

    Future<Integer> start();

    Integer getExitCode();
    
    void setExitCode(Integer exitCode);
    
    long getStartTime();
}
