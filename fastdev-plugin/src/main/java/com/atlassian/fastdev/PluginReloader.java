package com.atlassian.fastdev;

import com.atlassian.fastdev.maven.MavenTask;
import com.atlassian.fastdev.maven.MavenTaskManager;
import com.atlassian.fastdev.util.PomCLIChecker;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.MapMaker;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;
import java.util.concurrent.ConcurrentMap;

import static com.atlassian.fastdev.FastdevProperties.FASTDEV_PASSWORD_PROP;
import static com.atlassian.fastdev.FastdevProperties.FASTDEV_USERNAME_PROP;
import static com.atlassian.fastdev.util.Option.option;

/**
 * Builds and reloads plugins for a given root directory
 */
public class PluginReloader
{
    private final Logger LOG = LoggerFactory.getLogger(AutoReloadFilter.class);
    private final ApplicationProperties applicationProperties;
    private final MavenTaskManager taskManager;
    private final ConcurrentMap<String,Boolean> useCliForPom;

    public PluginReloader(ApplicationProperties applicationProperties,
                          MavenTaskManager taskManager)
    {
        this.applicationProperties = applicationProperties;
        this.taskManager = taskManager;
        this.useCliForPom = new MapMaker().makeMap();
    }

    public void pluginInstall(final ScanResult result)
    {
        if(useCli(result))
        {
            pluginInstallViaCLI(result);
        }
        else
        {
            pluginInstallViaMaven(result);
        }
    }

    public void pluginInstallWithTests(final ScanResult result)
    {
        if(useCli(result))
        {
            pluginInstallWithTestsViaCLI(result);
        }
        else
        {
            pluginInstallWithTestsViaMaven(result);
        }
    }

    private void pluginInstallViaMaven(final ScanResult result)
    {
        URI baseUri = URI.create(applicationProperties.getBaseUrl());
        int httpPort = baseUri.getPort();
        String contextPath = baseUri.getPath();

        File buildRoot = getBuildRoot(result);

        Builder<String> args = getBaseArgs(httpPort,contextPath);

        startTasks(buildRoot,args);
    }

    private void pluginInstallViaCLI(final ScanResult result)
    {
        URI baseUri = URI.create(applicationProperties.getBaseUrl());
        int httpPort = baseUri.getPort();
        String contextPath = baseUri.getPath();
        String host = baseUri.getHost();

        Builder<String> args = getCliArgs(httpPort,contextPath,host);

        startCliTasks(result,"pi",args);
    }
    
    private void pluginInstallWithTestsViaMaven(final ScanResult result)
    {
        URI baseUri = URI.create(applicationProperties.getBaseUrl());
        int httpPort = baseUri.getPort();
        String contextPath = baseUri.getPath();

        File buildRoot = getBuildRoot(result);

        Builder<String> args = getBaseArgsWithTests(httpPort, contextPath);

        startTasks(buildRoot,args);
    }

    private void pluginInstallWithTestsViaCLI(ScanResult result)
    {
        URI baseUri = URI.create(applicationProperties.getBaseUrl());
        int httpPort = baseUri.getPort();
        String contextPath = baseUri.getPath();
        String host = baseUri.getHost();

        Builder<String> args = getCliArgs(httpPort,contextPath,host);

        startCliTasks(result,"tpi",args);
    }

    private void startTasks(File buildRoot, Builder<String> args)
    {
        for (MavenTask task : taskManager.createTask(buildRoot, args.build()).right())
        {
            LOG.info("Executing mvn process in " + buildRoot.getPath());
            task.start();
        }
    }

    private void startCliTasks(ScanResult result, String command, Builder<String> args)
    {
        for (MavenTask task : taskManager.createCliTask(result, command, args.build()).right())
        {
            LOG.info("Executing mvn process in " + result.getBuildRoot().getPath());
            task.start();
        }
    }

    private Boolean useCli(ScanResult result)
    {
        File rootPom = result.getRootPom();
        boolean pomChanged = result.isPomChanged();

        if(pomChanged || !useCliForPom.containsKey(rootPom.getAbsolutePath()))
        {
            useCliForPom.put(rootPom.getAbsolutePath(),PomCLIChecker.useFastdevCli(rootPom));
        }
        
        return useCliForPom.get(rootPom.getAbsolutePath());
    }
    
    protected File getBuildRoot(ScanResult result)
    {
        if (!result.getBuildRoot().isDirectory() || !result.getRootPom().exists())
        {
            LOG.warn(result.getBuildRoot().getPath() + " does not contain pom.xml. Skipping");
        }
        
        return result.getBuildRoot();
    }
    
    protected Builder<String> getBaseArgs(int httpPort, String contextPath)
    {
        String ampsVersion = getAmpsVersion();
        Builder<String> args = ImmutableList.<String>builder().add(
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "copy-bundled-dependencies",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "compress-resources",
                "resources:resources",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "filter-plugin-descriptor",
                "compile",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "generate-manifest",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "jar",
                "org.apache.maven.plugins:maven-install-plugin:install",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "install",
                "-Dhttp.port=" + httpPort,
                "-Dcontext.path=" + contextPath);

        for (String username : option(System.getProperty(FASTDEV_USERNAME_PROP)))
        {
            args.add("-Dusername=" + username);
        }
        for (String password : option(System.getProperty(FASTDEV_PASSWORD_PROP)))
        {
            args.add("-Dpassword=" + password);
        }
        
        return args;
    }

    protected Builder<String> getBaseArgsWithTests(int httpPort, String contextPath)
    {
        String ampsVersion = getAmpsVersion();
        Builder<String> args = ImmutableList.<String>builder().add(
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "copy-bundled-dependencies",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "compress-resources",
                "resources:resources",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "filter-plugin-descriptor",
                "compile",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "generate-manifest",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "copy-test-bundled-dependencies",
                "resources:testResources",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "filter-test-plugin-descriptor",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "generate-test-manifest",
                "org.apache.maven.plugins:maven-compiler-plugin:" + ampsVersion + "testCompile",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "jar",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "test-jar",
                "org.apache.maven.plugins:maven-install-plugin:install",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "install",
                "com.atlassian.maven.plugins:maven-amps-plugin:" + ampsVersion + "test-install",
                "-Dhttp.port=" + httpPort,
                "-Dcontext.path=" + contextPath);

        for (String username : option(System.getProperty(FASTDEV_USERNAME_PROP)))
        {
            args.add("-Dusername=" + username);
        }
        for (String password : option(System.getProperty(FASTDEV_PASSWORD_PROP)))
        {
            args.add("-Dpassword=" + password);
        }

        return args;
    }

    protected Builder<String> getCliArgs(int httpPort, String contextPath, String host)
    {
        String ampsVersion = getAmpsVersion();
        Builder<String> args = ImmutableList.<String>builder().add(
                "com.atlassian.maven.plugins:maven-amps-dispatcher-plugin:" + ampsVersion + "cli",
                "-Dhttp.port=" + httpPort,
                "-Dcontext.path=" + contextPath);

        return args;
    }
    
    private String getAmpsVersion()
    {
        String sdkVersion = System.getProperty("atlassian.sdk.version","");
        String ampsVersion = "";
        if(StringUtils.isNotBlank(sdkVersion))
        {
            ampsVersion = sdkVersion + ":";
        }
        
        return ampsVersion;
    }
    
    protected String getAmpsProduct()
    {
        String ampsProduct = "amps";
        
        if(applicationProperties.getDisplayName().equalsIgnoreCase("jira"))
        {
            ampsProduct = "jira";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("confluence"))
        {
            ampsProduct = "confluence";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("bamboo"))
        {
            ampsProduct = "bamboo";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("crowd"))
        {
            ampsProduct = "crowd";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("stash"))
        {
            ampsProduct = "stash";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("fisheye") || applicationProperties.getDisplayName().equalsIgnoreCase("crucible") || applicationProperties.getDisplayName().equalsIgnoreCase("fecru"))
        {
            ampsProduct = "fecru";
        }

        if(applicationProperties.getDisplayName().equalsIgnoreCase("refapp"))
        {
            ampsProduct = "refapp";
        }
        
        return ampsProduct;
    }
}
