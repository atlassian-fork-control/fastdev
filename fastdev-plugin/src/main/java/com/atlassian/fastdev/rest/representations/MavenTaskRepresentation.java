package com.atlassian.fastdev.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.atlassian.fastdev.maven.MavenTask;
import com.atlassian.fastdev.rest.FastdevUriBuilder;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("unused")
public class MavenTaskRepresentation
{
    @JsonProperty private final Collection<String> output;
    @JsonProperty private final Map<String, URI> links;
    @JsonProperty private final String id;
    @JsonProperty private final Integer exitCode;
    @JsonProperty private final Long averageTaskTime;
    @JsonProperty private final Long elapsedTime;

    @JsonCreator
    public MavenTaskRepresentation(@JsonProperty("output") Collection<String> output,
                                   @JsonProperty("links") Map<String, URI> links,
                                   @JsonProperty("id") String id,
                                   @JsonProperty("exitCode") Integer exitCode,
                                   @JsonProperty("averageTaskTime") Long averageTaskTime,
                                   @JsonProperty("elapsedTime") Long elapsedTime)
    {
        this.output = ImmutableList.copyOf(output);
        this.links = ImmutableMap.copyOf(links);
        this.id = id;
        this.exitCode = exitCode;
        this.averageTaskTime = averageTaskTime;
        this.elapsedTime = elapsedTime;
    }

    public MavenTaskRepresentation(MavenTask task, FastdevUriBuilder uriBuilder)
    {
        this.output = ImmutableList.copyOf(task.getOutput());
        this.links = ImmutableMap.of("self", uriBuilder.buildMavenTaskUri(task));
        this.id = task.getUuid().toString();
        this.exitCode = task.getExitCode();
        this.averageTaskTime = task.getAverageTaskTime();
        this.elapsedTime = task.getElapsedTime();
    }
}
