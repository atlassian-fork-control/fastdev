package com.atlassian.livereload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.netiq.websocket.WebSocket;
import com.netiq.websocket.WebSocketServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * This is the LiveReload Websocket server implementation.
 * The only thing we really need to do here is start/stop the socket server and send the proper protocol hello message anytime a client connects.
 * 
 * We're extending WebSocketServer from the <a href="http://java-websocket.org/">Java-Websocket library</a> that does the heavy lifting for us.
 * We're using a fork of the library that simply adds maven support.
 * @see <a href="https://github.com/jribble/Java-WebSocket">https://github.com/jribble/Java-WebSocket</a>
 * 
 * @since 1.10
 */
public class LiveReloadServer extends WebSocketServer implements InitializingBean,DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(LiveReloadServer.class);
    
    //no need to parameterize this since it doesn't collide with LiveReload 2 and is only used internally
    public static final Integer DEFAULT_PORT = 35730;
    
    //The protocol we support
    public static final String PROTOCOL_7 = "http://livereload.com/protocols/official-7";
    
    private final String helloCommand;

    public LiveReloadServer()
    {
        //since the hello command never changes, just set it up once.
        Gson gson = new Gson();
        this.helloCommand = gson.toJson(new HelloCommand());
    }

    public void afterPropertiesSet() throws Exception
    {
        //startup the socket server
        this.setPort(DEFAULT_PORT);
        this.start();
        log.debug("started livereload server on port " + DEFAULT_PORT);
        
        //let's print a fun message to the logs, just because...
        System.out.print("___________            ________     ______            _________\n" +
                "___  /___(_)__   _________  __ \\_______  /___________ ______  /\n" +
                "__  / __  /__ | / /  _ \\_  /_/ /  _ \\_  /_  __ \\  __ `/  __  / \n" +
                "_  /___  / __ |/ //  __/  _, _//  __/  / / /_/ / /_/ // /_/ /  \n" +
                "/_____/_/  _____/ \\___//_/ |_| \\___//_/  \\____/\\__,_/ \\__,_/   \n" +
                "                                                               \n" +
                "__________             ______ ______    ______________         \n" +
                "___  ____/____________ ___  /____  /__________  /__  /         \n" +
                "__  __/  __  __ \\  __ `/_  __ \\_  /_  _ \\  __  /__  /          \n" +
                "_  /___  _  / / / /_/ /_  /_/ /  / /  __/ /_/ /  /_/           \n" +
                "/_____/  /_/ /_/\\__,_/ /_.___//_/  \\___/\\__,_/  (_)            \n" +
                "                                                               ");
    }

    public void destroy() throws Exception
    {
        this.stop();
        log.debug("stoppped livereload server");
    }

    public void onClientOpen(WebSocket webSocket)
    {
        try
        {
            //anytime a client connects, we need to send hello with the protocol we support
            webSocket.send(helloCommand);
        }
        catch (IOException e)
        {
            //we don't care
            e.printStackTrace();
        }
    }

    public void onClientClose(WebSocket webSocket)
    {
        //nothing to do, but he are forced to override this
    }

    public void onClientMessage(WebSocket webSocket, String s)
    {
        //nothing to do, but he are forced to override this
    }

    public void onError(Throwable t)
    {
        //nothing to do, but he are forced to override this
        t.printStackTrace();
    }

    static class HelloCommand
    {
        private String command;
        private List<String> protocols;
        private String serverName;

        HelloCommand()
        {
            this.command = "hello";
            this.protocols = new ArrayList<String>();
            protocols.add(PROTOCOL_7);
            this.serverName = "Atlassian Live Reload";
        }
    }
}
